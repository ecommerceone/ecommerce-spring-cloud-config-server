package com.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
public class EcommerceSpringCloudConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceSpringCloudConfigServerApplication.class, args);
		System.out.println("spring-cloud-config-server is running");
	}

}
